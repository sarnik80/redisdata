package main

import (
	"fmt"
	"io"
	"net/http"

	"github.com/go-redis/redis"
)

var client *redis.Client

func init() {
	client = redis.NewClient(&redis.Options{

		Addr: "redisi:6379",

		Password: "",

		DB: 0,
	})

}

func main() {

	http.HandleFunc("/", index)

	http.ListenAndServe(":9090", nil)
}

func index(w http.ResponseWriter, r *http.Request) {

	username, err := client.Get("name").Result()

	if err != nil {
		fmt.Println(err)
	}

	result := "Hello " + username + "\n"

	io.WriteString(w, result)
}
